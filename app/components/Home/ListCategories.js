import React from "react";
import {
	FlatList,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
} from "react-native";

export default function ListCategories(props) {
	const { categories, id, navigation } = props;

	return (
		<View>
			{categories ? (
				<FlatList
					data={categories}
					renderItem={(category) => (
						<Category
							category={category}
							id={id}
							navigation={navigation}
						/>
					)}
					keyExtractor={(item, index) => index.toString()}
				></FlatList>
			) : (
				<View />
			)}
		</View>
	);
}

function Category(props) {
	const { category, navigation } = props;
	const { id } = category.item.category;
	const { en } = category.item.category.name;

	return (
		<TouchableOpacity
			onPress={() => navigation.navigate("ReportPriceSourcing", { id })}
		>
			<View style={styles.categoryView}>
				<Text style={styles.categoryText}>_ {en}</Text>
			</View>
		</TouchableOpacity>
	);
}

const styles = StyleSheet.create({
	categoryView: {
		flexDirection: "row",
		padding: 48,
	},
	categoryText: {
		color: "#1D102F",
	},
});
