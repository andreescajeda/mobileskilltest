import React, { useEffect, useState } from "react";
import {
	FlatList,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
} from "react-native";

export default function ListSubcategories(props) {
	const { id, navigation, subcategories } = props;

	return (
		<View>
			{subcategories ? (
				<FlatList
					data={subcategories}
					renderItem={(subcategory) => (
						<Subcategory
							id={id}
							navigation={navigation}
							subcategory={subcategory}
						/>
					)}
					keyExtractor={(item, index) => index.toString()}
				></FlatList>
			) : (
				<View />
			)}
		</View>
	);
}

function Subcategory(props) {
	const { id, navigation, subcategory } = props;
	const { en } = subcategory.item.subcategory.name;

	return (
		<TouchableOpacity
			onPress={() =>
				navigation.navigate("Description", {
					en,
					id,
					navigation,
					subcategory,
				})
			}
		>
			<View style={styles.categoryView}>
				<Text style={styles.categoryText}>_ {en}</Text>
			</View>
		</TouchableOpacity>
	);
}

const styles = StyleSheet.create({
	categoryView: {
		flexDirection: "row",
		padding: 48,
	},
	categoryText: {
		color: "#1D102F",
	},
});
