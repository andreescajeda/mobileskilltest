import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { firebaseApp } from "../utils/Firebase";
import firebase from "firebase/app";
import "firebase/firestore";
import ListCategories from "../components/Home/ListCategories";

const db = firebase.firestore(firebaseApp);

export default function Home(props) {
	const { navigation } = props;
	const [categories, setCategories] = useState([]);

	useEffect(() => {
		(async () => {
			await db
				.collection("categories")
				.get()
				.then((snapshot) => {
					const resultCategories = [];

					snapshot.forEach((doc) => {
						let category = doc.data();
						category.id = doc.id;
						resultCategories.push({ category });
					});

					setCategories(resultCategories.reverse());
				})
				.catch((err) => {
					console.log(err);
				});
		})();
	}, []);

	return (
		<View style={styles.viewport}>
			<Text style={styles.titleText}>Category</Text>
			<Text style={styles.subtitleText}>
				Select an option to continue
			</Text>
			<ListCategories categories={categories} navigation={navigation} />
		</View>
	);
}

const styles = StyleSheet.create({
	// Button
	backButton: {
		color: "#CA243D",
	},
	nextButton: {
		backgroundColor: "#CA243D",
	},
	// View
	container: {
		flex: 1,
		backgroundColor: "#F6F2FF",
	},
	viewport: {
		marginTop: 16,
		marginRight: 16,
		marginLeft: 16,
	},
	// Text
	titleText: {
		color: "#1D102F",
		fontWeight: "bold",
	},
	subtitleText: {
		color: "#1D102F",
	},
});
