import React from "react";
import { View, Text } from "react-native";

export default function PersonalInfo() {
	return (
		<View style={styles.container}>
			<Text style={styles.titleText}>Category</Text>
			<Text style={styles.subtitleText}>
				Select an option to continue
			</Text>
		</View>
	);
}
