import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { firebaseApp } from "../utils/Firebase";
import firebase from "firebase/app";
import "firebase/firestore";
import ListSubcategories from "../components/ReportPriceGouging/ListSubcategories";

const db = firebase.firestore(firebaseApp);

export default function ReportPriceSourcing(props) {
	const { navigation } = props;
	const { id } = props.route.params;
	const [subcategories, setSubcategories] = useState([]);

	useEffect(() => {
		(async () => {
			await db
				.collection("categories/" + id + "/subcategories")
				.get()
				.then((snapshot) => {
					const resultSubcategories = [];

					snapshot.forEach((doc) => {
						let subcategory = doc.data();
						subcategory.id = doc.id;
						resultSubcategories.push({ subcategory });
					});

					setSubcategories(resultSubcategories.reverse());
				})
				.catch((err) => {
					console.log(err);
				});
		})();
	}, []);

	return (
		<View style={styles.viewport}>
			<Text style={styles.titleText}>Report Price Gouging</Text>
			<Text style={styles.subtitleText}>
				Please select the type of product or service that is being sold
				at an exorbitant or excesive price
			</Text>
			<ListSubcategories
				id={id}
				navigation={navigation}
				subcategories={subcategories}
			/>
		</View>
	);
}

const styles = StyleSheet.create({
	// Button
	backButton: {
		color: "#CA243D",
	},
	nextButton: {
		backgroundColor: "#CA243D",
	},
	// View
	container: {
		flex: 1,
		backgroundColor: "#F6F2FF",
	},
	viewport: {
		marginTop: 16,
		marginRight: 16,
		marginLeft: 16,
	},
	// Text
	titleText: {
		color: "#1D102F",
		fontWeight: "bold",
	},
	subtitleText: {
		color: "#1D102F",
	},
});
