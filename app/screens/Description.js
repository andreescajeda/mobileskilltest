import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Button } from "react-native-paper";
import { Textarea, Form } from "native-base";

export default function Description(props) {
	const { en, id, navigation } = props;
	const [comments, setComments] = useState("");

	return (
		<View style={styles.viewport}>
			<Text style={styles.titleText}>Report Price Gouging</Text>
			<Text style={styles.subtitleText}>
				Please provide a Brief Description of a issue.
			</Text>
			<Form>
				<Textarea
					bordered
					onChange={(e) => setComments(e.nativeEvent.text)}
					placeholder="Type here, 200 characters or 29 words allowed."
					rowSpan={5}
				/>
			</Form>
			<View style={styles.layout}>
				<View style={styles.flexbox}>
					<Button
						color="#CA243D"
						mode="contained"
						onPress={() =>
							navigation.navigate("Map", {
								comments,
								en,
								id,
								navigation,
							})
						}
						style={styles.nextButton}
					>
						Next
					</Button>
				</View>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	// Button
	backButton: {
		color: "#CA243D",
	},
	nextButton: {
		backgroundColor: "#CA243D",
	},
	// View
	container: {
		flex: 1,
		backgroundColor: "#F6F2FF",
	},
	// Text
	titleText: {
		color: "#1D102F",
		fontWeight: "bold",
	},
	subtitleText: {
		color: "#1D102F",
	},
	// View
	layout: {
		alignItems: "center",
		flex: 1,
		flexDirection: "row",
		justifyContent: "center",
		marginTop: 16,
	},
	flexbox: {
		flex: 1,
	},
	viewport: {
		flex: 1,
		marginTop: 16,
		marginRight: 16,
		marginLeft: 16,
	},
});
