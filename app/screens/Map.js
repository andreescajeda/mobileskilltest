import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import MapView from "react-native-maps";
import { Button, TextInput } from "react-native-paper";
import * as Location from "expo-location";
import * as Permissions from "expo-permissions";
import Geocoder from "react-native-geocoding";

export default function Map(props) {
	const { comments, en, id, navigation } = props;
	const [location, setLocation] = useState(null);

	const confirmLocation = () => {
		setLocation(location);

		let latLng = {
			lat: location.latitude,
			lng: location.longitude,
		};

		Geocoder.init("AIzaSyDqdmDOdohntytRaxMOWtBEtagBNdwRdN8", {
			language: "en",
		});

		Geocoder.from(latLng.lat, latLng.lng)
			.then((json) => {
				const results = json.results[0];

				navigation.navigate("SelfScreeningTool", {
					comments,
					en,
					id,
					results,
				});
			})
			.catch((error) => console.warn(error));
	};

	useEffect(() => {
		(async () => {
			const resultPermissions = await Permissions.askAsync(
				Permissions.LOCATION
			);
			const statusPermissions =
				resultPermissions.permissions.location.status;

			if (statusPermissions !== "granted") {
				console.log("enable geolocation");
			} else {
				const loc = await Location.getCurrentPositionAsync({});

				setLocation({
					latitude: loc.coords.latitude,
					longitude: loc.coords.longitude,
					latitudeDelta: 0.001,
					longitudeDelta: 0.001,
				});
			}
		})();
	}, []);

	return (
		<View style={styles.container}>
			{location && (
				<MapView
					initialRegion={location}
					onRegionChange={(region) => setLocation(region)}
					showsUserLocation={true}
					style={styles.mapView}
				>
					<MapView.Marker
						coordinate={{
							latitude: location.latitude,
							longitude: location.longitude,
						}}
						draggable
					/>
				</MapView>
			)}
			<TextInput
				label="Type your location"
				mode="outlined"
				style={styles.actionButton}
			/>
			<View style={styles.layout}>
				<View style={styles.flexbox}>
					<Button
						color="#CA243D"
						mode="contained"
						onPress={() => confirmLocation()}
						style={styles.nextButton}
					>
						Next
					</Button>
				</View>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	image: {
		backgroundColor: "#F6F2FF",
		height: "100%",

		position: "absolute",
		width: "100%",
	},
	// View
	innerView: {
		marginTop: 16,
	},
	mapView: {
		width: "100%",
		height: "100%",
	},
	actionButton: {
		backgroundColor: "#F6F2FF",
		justifyContent: "center",
		marginTop: 16,
		marginLeft: "5%",
		position: "absolute",
		width: "90%",
	},
	// View
	layout: {
		alignItems: "center",
		backgroundColor: "#F6F2FF",
		bottom: 16,
		flex: 1,
		flexDirection: "row",
		justifyContent: "center",
		marginLeft: "5%",
		position: "absolute",
		width: "90%",
	},
	flexbox: {
		flex: 1,
	},
});
