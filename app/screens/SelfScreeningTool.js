import React, { useState } from "react";
import { Alert, ScrollView, StyleSheet, Text, View } from "react-native";
import { Avatar } from "react-native-elements";
import { Button, TextInput } from "react-native-paper";
import { validateEmail } from "../utils/Validators";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import uuid from "random-uuid-v4";
import { firebaseApp } from "../utils/Firebase";
import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/storage";

const db = firebase.firestore(firebaseApp);

export default function SelfScreeningTool(props) {
	const { comments, en, id, results } = props.route.params;
	console.log(results);

	const [email, setEmail] = useState("");
	const [phone, setPhone] = useState("");
	const [uri, setURI] = useState("");

	const selectImage = async () => {
		const resultPermission = await Permissions.askAsync(
			Permissions.CAMERA_ROLL
		);

		const resultPermissionCamera =
			resultPermission.permissions.cameraRoll.status;

		if (resultPermissionCamera === "denied") {
			console.log(resultPermissionCamera);
		} else {
			const result = await ImagePicker.launchImageLibraryAsync({
				allowsEditing: true,
				aspect: [1, 1],
			});

			if (result.cancelled) {
				console.log("closed");
			} else {
				setURI(result.uri);
			}
		}
	};

	const removeImage = (uri) => {
		Alert.alert(
			"Remove Image",
			"Are you sure you want to remove the image?",
			[
				{
					style: "cancel",
					text: "Cancel",
				},
				{
					onPress: () => setURI(""),
					style: "destructive",
					text: "Remove",
				},
			]
		);
	};

	const submit = () => {
		if (!email || !phone) {
			console.log("empty");
		} else {
			if (!validateEmail(email)) {
				console.log("incorrect");
			} else {
				if (uri) {
					uploadImageToStorage(uri).then((name) => {
						db.collection("reports")
							.add({
								adr: results.formatted_address,
								adc: {
									c: results.address_components[4].long_name,
									cry:
										results.address_components[7].long_name,
									cty:
										results.address_components[5].long_name,
									pc: results.address_components[8].long_name,
									s: results.address_components[6].long_name,
								},
								cS: "Pending",
								cId: "",
								created: new Date(),
								ef: "",
								im: name,
								pos: {
									lat: results.geometry.location.lat,
									lng: results.geometry.location.lng,
								},
								tn: "",
								ue: email,
								ph: phone,
								na: "",
								des: "",
								fo: "Mobile",
							})
							.then(() => {
								console.log("success");
							})
							.catch((err) => {
								console.log(err);
							});
					});
				}
			}
		}
	};

	const uploadImageToStorage = async (uri) => {
		var name = "";
		const response = await fetch(uri);
		const blob = await response.blob();
		const ref = firebase.storage().ref("im").child(uuid());

		await ref
			.put(blob)
			.then((res) => {
				name = res.metadata.name;
			})
			.catch((err) => {
				console.log(err);
			});

		return name;
	};

	return (
		<ScrollView style={styles.viewport}>
			<Text style={styles.titleText}>Personal Information</Text>
			<TextInput
				placeholder="Email"
				mode="outlined"
				onChange={(e) => setEmail(e.nativeEvent.text)}
				style={styles.textInput}
			/>
			<TextInput
				input="number"
				label="Phone number"
				mode="outlined"
				onChange={(e) => setPhone(e.nativeEvent.text)}
				style={styles.textInput}
			/>
			<View style={styles.innerView}>
				<Avatar
					onPress={() => removeImage()}
					source={{ uri: uri }}
					style={styles.avatar}
				/>
				{uri.length === 0 && (
					<View style={styles.image}>
						<Button
							color="#1D102F"
							mode="outlined"
							onPress={() => selectImage()}
							style={styles.libraryButton}
							uppercase="false"
						>
							Library
						</Button>
						<Button
							color="#1D102F"
							disabled
							mode="outlined"
							style={styles.takeAPictureButton}
							uppercase="false"
						>
							Take a picture
						</Button>
					</View>
				)}
			</View>
			<View style={styles.layout}>
				<View style={styles.flexbox}>
					<Button
						color="#CA243D"
						mode="contained"
						onPress={() => submit()}
						style={styles.nextButton}
					>
						Submit
					</Button>
				</View>
			</View>
		</ScrollView>
	);
}

const styles = StyleSheet.create({
	// Avatar
	avatar: {
		aspectRatio: 1,
		backgroundColor: "#F6F2FF",
	},
	// Button
	backButton: {
		flex: 1,
		marginEnd: 8,
	},
	libraryButton: {
		color: "#1D102F",
		width: "50%",
	},
	submitButton: {
		flex: 1,
		marginStart: 8,
	},
	takeAPictureButton: {
		color: "#1D102F",
		marginTop: 8,
		width: "50%",
	},
	// TextInput
	textInput: {
		backgroundColor: "#F6F2FF",
		marginTop: 8,
	},
	// View
	innerView: {
		marginTop: 16,
	},
	image: {
		alignItems: "center",
		backgroundColor: "#F6F2FF",
		height: "100%",
		justifyContent: "center",
		position: "absolute",
		width: "100%",
	},
	viewport: {
		marginTop: 16,
		marginRight: 16,
		marginLeft: 16,
	},
	layout: {
		alignItems: "center",
		flex: 1,
		flexDirection: "row",
		justifyContent: "center",
		marginTop: 16,
	},
	flexbox: {
		flex: 1,
	},
});
