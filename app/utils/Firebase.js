import firebase from "firebase/app";

let firebaseConfig = {
	apiKey: "AIzaSyCr2zBx7sDStWsiKK206tWxBO5my9i7pxM",
	authDomain: "mobileskilltest.firebaseapp.com",
	databaseURL: "https://mobileskilltest.firebaseio.com",
	projectId: "mobileskilltest",
	storageBucket: "mobileskilltest.appspot.com",
	messagingSenderId: "871318530599",
	appId: "1:871318530599:web:1c0d617df94bb44433bc02",
};

export let firebaseApp = firebase.initializeApp(firebaseConfig);
