import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Home from "./app/screens/Home";
import Description from "./app/screens/Description";
import SelfScreeningTool from "./app/screens/SelfScreeningTool";
import Map from "./app/screens/Map";
import ReportPriceSourcingScreen from "./app/screens/ReportPriceSourcing";

let Stack = createStackNavigator();

function App() {
	return (
		<NavigationContainer theme={Theme}>
			<Stack.Navigator>
				<Stack.Screen
					name="Home"
					component={Home}
					options={{
						title: "COVID-19 Support",
					}}
				/>
				<Stack.Screen
					name="ReportPriceSourcing"
					component={ReportPriceSourcingScreen}
					options={{
						title: "COVID-19 Support",
					}}
				/>
				<Stack.Screen
					name="Description"
					component={Description}
					options={{
						title: "COVID-19 Support",
					}}
				/>
				<Stack.Screen
					name="Map"
					component={Map}
					options={{
						title: "COVID-19 Support",
					}}
				/>
				<Stack.Screen
					name="SelfScreeningTool"
					component={SelfScreeningTool}
					options={{
						title: "COVID-19 Support",
					}}
				/>
			</Stack.Navigator>
		</NavigationContainer>
	);
}

export default App;

const Theme = {
	dark: false,
	colors: {
		primary: "#1D102F",
		background: "#F6F2FF",
		card: "#F6f2FF",
		text: "#1D102F",
		border: "#CAC7D6",
	},
};
